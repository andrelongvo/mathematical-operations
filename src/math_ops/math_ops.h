#include <string>
#include <vector>

namespace Operations {
    class Math_Ops {
        public:
            double add(const std::vector<double> &numbers);
            double subtract(const std::vector<double> &numbers);
            double multiply(const std::vector<double> &numbers);
            double divide(const std::vector<double> &numbers);
    };
}