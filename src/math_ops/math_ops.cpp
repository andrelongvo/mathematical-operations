#include "math_ops.h"

using namespace Operations;

double Math_Ops::add(const std::vector<double> &numbers) {
    double total = numbers[0];
    for (int i = 1; i < numbers.size(); i++) {
        total += numbers[i];
    }
    return total;
}

double Math_Ops::subtract(const std::vector<double> &numbers) {
    double total = numbers[0];
    for (int i = 1; i < numbers.size(); i++) {
        total -= numbers[i];
    }
    return total;
}

double Math_Ops::multiply(const std::vector<double> &numbers) {
    double total = numbers[0];
    for (int i = 1; i < numbers.size(); i++) {
        total *= numbers[i];
    }
    return total;
}

double Math_Ops::divide(const std::vector<double> &numbers) {
    double total = numbers[0];
    for (int i = 1; i < numbers.size(); i++) {
        total /= numbers[i];
    }
    return total;
}
