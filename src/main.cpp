#include <iostream>
#include <string>
#include <vector>

#include "math_ops/math_ops.h"

/*
    === MAKE SURE TO SET TARGET TO PARENT DIRECTORY ===
    IDE used: Visual Studio Code
    
    compile cmd: g++ -o exercise_executable src/main.cpp src/math_ops/math_ops.cpp
    run: a.exe -operation number number

    cmake: cmake --build ./build
    CMake executable build will be named "exercise_executable.exe" within build/src

    TODO:
    - function to ensure number arguments are valid numbers; assumes valid number input
*/

using namespace Operations;

static void show_help(std::string file_name) {
    std::cerr << "Usage: " << file_name << " --OPERATION NUMBER NUMBER\n"
              << "Provide at least two numeric inputs\n"
                  << "Operations:\n"
                  << "\t-a,--add\t\tAdd numbers\n"
                  << "\t-s,--subtract\t\tSubtract numbers\n"
                  << "\t-m,--multiply\t\tMultiply numbers\n"
                  << "\t-d,--divide\t\tDivide numbers"
                  << std::endl;
}

int main(int argc, char* argv[]) {
    if (argc < 4) { //looks for at least four arguments: file_name operation number number
        show_help(argv[0]);
        return 1;
    }

    Math_Ops ops;
    std::string operation = argv[1];
    std::vector<double> numbers;
    for (int i = 2; i < argc; i++) {
        numbers.push_back(std::stod(argv[i]));
    }

    if ((operation == "-a") || (operation == "--add")) {
        std::cout << ops.add(numbers);
    } else if ((operation == "-s") || (operation == "--subtract")) {
        std::cout << ops.subtract(numbers);
    } else if ((operation == "-m") || (operation == "--multiply")) {
        std::cout << ops.multiply(numbers);
    } else if ((operation == "-d") || (operation == "--divide")) {
        std::cout << ops.divide(numbers);
    } else {
        show_help(argv[0]);
        return 1;
    }
    return 0;
}