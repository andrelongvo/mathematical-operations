# Mathematical Operations

### C++ coding exercise built and compiled with CMake 3.17.1 and the Visual Studio Code IDE.

---

Compile: `g++ -o exercise_executable src/main.cpp src/math_ops/math_ops.cpp`\
CMake: `cmake --build ./build`

Usage: `exercise_executable.exe --operation number number`\
Provide at least two numeric inputs\
Operations:\
`-a` `--add`\
`-s` `--subtract`\
`-m` `--multiply`\
`-d` `--divide`